package org.paititi.datasource;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class App {

	public static void main(String[] args) throws Exception {
		final Properties properties = new Properties();
		properties.load(new FileInputStream(new File(args[0])));
		for (final Object key : properties.keySet()) {
			new Thread() {
				public void run() {
					Calendar calendar = Calendar.getInstance();
					ZerodhaPullMarketData marketData = new ZerodhaPullMarketData(
							(String) key, Integer.parseInt((String) properties
									.getProperty((String) key)));
					String dir = "/home/ankush/zerodha_data/" + new SimpleDateFormat("YYYY-MM-dd").format(calendar.getTime()) + "/" + key + "/";
					new File(dir).mkdirs();
					marketData.logL2Data(dir + "book.dat");
				}
			}.start();
		}
	}
}
