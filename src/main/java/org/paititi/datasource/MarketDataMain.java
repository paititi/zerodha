package org.paititi.datasource;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.MembershipKey;
import org.paititi.utils.IPv4Address;

public class MarketDataMain {
	private DatagramChannel channel;
	private InetAddress     group;
	private int             port;
	
	public MarketDataMain(IPv4Address address) throws Exception {
		// Poor's man formula to get interface!
		NetworkInterface ni = NetworkInterface.getNetworkInterfaces()
				.nextElement();

		channel = DatagramChannel.open(StandardProtocolFamily.INET)
				.setOption(StandardSocketOptions.SO_REUSEADDR, true)
				.bind(new InetSocketAddress(address.port()))
				.setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
		group = InetAddress.getByName(address.address());
		port = address.port();
		@SuppressWarnings("unused")
		MembershipKey key = channel.join(group, ni);
	}

	public int broadCast(byte [] data) throws Exception {
		ByteBuffer buf = ByteBuffer.allocate(16000);
		buf.clear();
		buf.put(data);
		buf.flip();
		return channel.send(buf, new InetSocketAddress(group, port));
	}
	
	public void run() {
		// One thread should be dedicated for Zerodha L1 data
		// Another thread would get L2 data from either ICICI or Zerodha.
		// There should be a low priority thread to send L2 data on request
	}
}
