/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.paititi.datasource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.paititi.messages.*;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableBody;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.google.protobuf.TextFormat;

/**
 * 
 * @author rsaraf
 */
public class ICICIMarketData {

	private final String uri_base = "http://getquote.icicidirect.com/trading/equity/trading_stock_bestbid.asp?Symbol=";

	private final WebClient webclient;

	public ICICIMarketData() {
		webclient = new WebClient();
	}

	public void updateData(String symbol) throws IOException {
		String uri = uri_base + symbol;
		HtmlPage p = webclient.getPage(uri);
		List<?> list;
		list = p.getByXPath("/html/body/table[1]/tbody/tr[1]/td//table/tbody");
		HtmlTableBody table_body = (HtmlTableBody) list.get(list.size() - 1);
		list = table_body
				.getByXPath("/html/body/table[1]/tbody/tr[1]/td//table//tbody//tr");
		MarketDataL2Message.book.Builder bookProto = MarketDataL2Message.book.newBuilder();
		bookProto.setSymbol(symbol);
		for (int i = 12; i <= 18; i++) {
			MarketDataL2Message.book.level.Builder askLevelBuilder = MarketDataL2Message.book.level
					.newBuilder();
			MarketDataL2Message.book.level.Builder bidLevelBuilder = MarketDataL2Message.book.level
					.newBuilder();

			HtmlTableRow row = (HtmlTableRow) list.get(i);
			String str = row.asText().replace("*", "").replace(",", "").trim();
			String[] data = str.split("[\\s+]");
			System.out.println(Arrays.toString(data));
			if (i == 17) {
				if (!"N.A".equals(data[3])) {
					bookProto.setTotalBidQty(Long.parseLong(data[3]));
				} else {
					bookProto.setTotalBidQty(0);
				}
				continue;
			}
			if (i == 18) {
				if (!"N.A".equals(data[3])) {
					bookProto.setTotalAskQty(Long.parseLong(data[3]));
				} else {
					bookProto.setTotalAskQty(0);
				}
				continue;
			}
			askLevelBuilder.setQty(Integer.parseInt(data[2]));
			askLevelBuilder.setPrice(Double.parseDouble(data[3]));
			bidLevelBuilder.setQty(Integer.parseInt(data[0]));
			bidLevelBuilder.setPrice(Double.parseDouble(data[1]));
			bookProto.addBuyLevel(bidLevelBuilder);
			bookProto.addSellLevel(askLevelBuilder);
		}
		MarketDataL2Message.book book = bookProto.build();
		TextFormat.printToString(book);
	}
/*
	public static void main(String[] args) throws IOException {
		ICICIMarketData md = new ICICIMarketData();
		md.updateData("TATTEA");
	} */
}
