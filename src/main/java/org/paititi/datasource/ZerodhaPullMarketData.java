package org.paititi.datasource;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.paititi.messages.*;
import org.paititi.utils.DataLogger;

public class ZerodhaPullMarketData {

	private String uri = "https://trade.zerodha.com/Nest3New/PlaceOrder/AjaxQuote.jsp?Exc=nse_cm&Trns=Buy&Sym=";
	private String symbol;
	private static final long MARKET_END_TIME = 930*60*1000L;
	volatile boolean running = true;

	
	public ZerodhaPullMarketData(String symbol, int zerodhaID) {
		this.symbol = symbol;
		this.uri += zerodhaID;
	}
	
	public void logL2Data(String filePath) {
		try {
			final DataLogger<MarketDataL2Message.book> logger = new DataLogger<MarketDataL2Message.book>(filePath);
			final LinkedBlockingQueue<MarketDataL2Message.book> queue = new LinkedBlockingQueue<MarketDataL2Message.book>();
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			MarketDataL2Message.book.level.Builder bidLevelBuilder = MarketDataL2Message.book.level
					.newBuilder();
			MarketDataL2Message.book.level.Builder askLevelBuilder = MarketDataL2Message.book.level
					.newBuilder();
			MarketDataL2Message.book.Builder bookProto = MarketDataL2Message.book.newBuilder();

			URL url = new URL(uri);
			System.out.println("Started logging data for " + uri);
			ByteBuffer buffer = ByteBuffer.allocate(1024*10);
			ReadableByteChannel rbc = Channels.newChannel(url.openStream());
			int numBytes = rbc.read(buffer);
						
			Thread t = new Thread() {
				List<MarketDataL2Message.book> list = new LinkedList<MarketDataL2Message.book>();
				public void run() {
					while(running) {
						processList();
					}
					processList();
				}
				
				public void processList() {
					queue.drainTo(list);
					if(!list.isEmpty()) {
						for(MarketDataL2Message.book book : list) {
							try {
								logger.dumpProto(book);
							} catch (IOException e) {
								e.printStackTrace();
							}
							list.clear();
						}
					}
				}
			};
			t.start();
			
			while((System.currentTimeMillis() - calendar.getTimeInMillis()) <= MARKET_END_TIME) {
				try {
					long startTime = System.currentTimeMillis();
					buffer.clear();
					rbc = Channels.newChannel(url.openStream());
					numBytes = rbc.read(buffer);
					rbc.close();
					long readTime = System.currentTimeMillis() - startTime;
					String str = new String(buffer.array(), 0, numBytes);
					bookProto.clear();
					bookProto.setPreviousClosePrice(readTime);
					String[] data = str.split("[|]");
					bookProto.setSymbol(symbol);
					bookProto.setTime(System.currentTimeMillis() - calendar.getTimeInMillis());
					bookProto.setLtp(Double.parseDouble(data[32]));
					bookProto.setLtq(Integer.parseInt(data[36]));
					bookProto.setTotalVolume(Long.parseLong(data[41]));
					bookProto.setTotalBidQty(Long.parseLong(data[48]));
					bookProto.setTotalAskQty(Long.parseLong(data[49]));
					bookProto.setVwapPx(Double.parseDouble(data[40]));
					for(int i=0; i<5; i++) {
						bidLevelBuilder.clear();
						askLevelBuilder.clear();
						
						bidLevelBuilder.setPrice(Double.parseDouble(data[i]));
						bidLevelBuilder.setNumOrders(Integer.parseInt(data[i+10]));
						bidLevelBuilder.setQty(Integer.parseInt(data[i+20]));
						bookProto.addBuyLevel(bidLevelBuilder.build());
						
						askLevelBuilder.setPrice(Double.parseDouble(data[i+5]));
						askLevelBuilder.setNumOrders(Integer.parseInt(data[i+15]));
						askLevelBuilder.setQty(Integer.parseInt(data[i+25]));
						bookProto.addSellLevel(askLevelBuilder.build());
					}
					long timeTaken = System.currentTimeMillis() - startTime;
					bookProto.setOpenInterest(timeTaken);
					MarketDataL2Message.book book = bookProto.build();	
					queue.add(book);
					Thread.sleep(Math.max(100 - timeTaken, 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			running = false;
			t.join();
			logger.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
