package org.paititi.utils;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.util.CharsetUtil;

import java.util.Calendar;

public class NettyClient {

	static String URL = "http://trade.zerodha.com/Nest3New/PlaceOrder/AjaxQuote.jsp?Exc=nse_cm&Trns=Buy&Sym=3351";

	public static void main(String[] args) throws Exception {
		System.setErr(null);
		// URI uri = new URI(URL);
		// String scheme = uri.getScheme() == null ? "http" : uri.getScheme();
		// String host = uri.getHost() == null ? "127.0.0.1" : uri.getHost();
		// int port = uri.getPort();
		// if (port == -1) {
		// if ("http".equalsIgnoreCase(scheme)) {
		// port = 80;
		// } else if ("https".equalsIgnoreCase(scheme)) {
		// port = 443;
		// }
		// }
		//
		// if (!"http".equalsIgnoreCase(scheme) &&
		// !"https".equalsIgnoreCase(scheme)) {
		// System.err.println("Only HTTP(S) is supported.");
		// return;
		// }

		// Configure SSL context if necessary.
		final boolean ssl = false; // "https".equalsIgnoreCase(scheme);
		final SslContext sslCtx;
		if (ssl) {
			sslCtx = SslContext
					.newClientContext(InsecureTrustManagerFactory.INSTANCE);
		} else {
			sslCtx = null;
		}

		String host = "trade.zerodha.com";
		int port = 80;
		// Configure the client.
		// while(true) {
		EventLoopGroup group = new NioEventLoopGroup(1);
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioSocketChannel.class)
					.handler(new HttpSnoopClientInitializer(sslCtx));

			// Make the connection attempt.
			final Channel ch = b.connect(host, port).sync().channel();

			// Prepare the HTTP request.
			final HttpRequest request = new DefaultFullHttpRequest(
					HttpVersion.HTTP_1_1, HttpMethod.GET, URL);
			request.headers().set(HttpHeaders.Names.HOST, host);
			request.headers().set(HttpHeaders.Names.CONNECTION,
					HttpHeaders.Values.KEEP_ALIVE);
			// request.headers().set(HttpHeaders.Names.ACCEPT_ENCODING,
			// HttpHeaders.Values.GZIP);

			// Send the HTTP request.

			new Thread() {
				public void run() {
					while(true) {
						ch.writeAndFlush(request);
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
						}
					}
				}
			}.start();
			// Wait for the server to close the connection.
			ch.closeFuture().sync();
		} finally {
			// Shut down executor threads to exit.
			group.shutdownGracefully();
		}
		// }
	}

	public static class HttpSnoopClientInitializer extends
			ChannelInitializer<SocketChannel> {

		private final SslContext sslCtx;

		public HttpSnoopClientInitializer(SslContext sslCtx) {
			this.sslCtx = sslCtx;
		}

		@Override
		public void initChannel(SocketChannel ch) {
			ChannelPipeline p = ch.pipeline();

			// Enable HTTPS if necessary.
			if (sslCtx != null) {
				p.addLast(sslCtx.newHandler(ch.alloc()));
			}

			p.addLast(new HttpClientCodec());

			p.addLast(new HttpSnoopClientHandler());
		}
	}

	public static class HttpSnoopClientHandler extends
			SimpleChannelInboundHandler<HttpObject> {

		volatile long i = 0;

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
			 cause.printStackTrace();
			// ctx.close();
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg)
				throws Exception {

			if (msg instanceof HttpContent) {
				HttpContent content = (HttpContent) msg;

				// ByteBuf buf = content.content();
				// String str = "";
				// while (buf.readableBytes() > 1) {
				// str += (char) (buf.readChar() & 0xFF);
				// }
				// System.out.println(str);
				 if ((i++ % 100) == 0) System.out.println(i);
//				System.out.println(content.content()
//						.toString(CharsetUtil.UTF_8));
				// }
				// if (content instanceof LastHttpContent) {
				// System.err.println("} END OF CONTENT");
				// ctx.close();
				// }
			}

		}
	}

}
