/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.paititi.utils;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import com.google.protobuf.Message;

/**
 * 
 * @author rsaraf
 * @param <T>
 */

public class DataLogger<T extends Message> {
	private final String outFile;
	private final DataOutputStream outStream;
	private final long startOfDayMS;

	public DataLogger(String outFile) throws FileNotFoundException {
		this.outFile = outFile;
		this.outStream = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(outFile), 1024 * 1024));
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		startOfDayMS = calendar.getTimeInMillis();
	}

	public static class DataHeader {
		private int numBytes;

		public void writeFields(DataOutputStream out) throws IOException {
			out.writeInt(getNumBytes());
		}

		public void readFields(DataInputStream in) throws IOException {
			this.setNumBytes(in.readInt());
		}

		public int getNumBytes() {
			return numBytes;
		}

		public void setNumBytes(int numBytes) {
			this.numBytes = numBytes;
		}
	}

	public void dumpProto(T messge) throws IOException {
		DataHeader dataHeader = new DataHeader();
		dataHeader.setNumBytes(messge.getSerializedSize());
		dataHeader.writeFields(getOutStream());
		getOutStream().write(messge.toByteArray());
	}

	public void close() throws IOException {
		getOutStream().close();
	}

	public String getOutFile() {
		return outFile;
	}

	public DataOutputStream getOutStream() {
		return outStream;
	}

	public long getStartOfDayMS() {
		return startOfDayMS;
	}
}
