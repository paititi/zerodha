package org.paititi.utils;

import java.io.IOException;
import java.nio.channels.Channel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

public class SelectDispatcher {
	private Selector selector;
	private boolean  running;
	
	public interface ChannelProcessor {
		public void process(Channel channel, SelectionKey key);
	}
	
	public SelectDispatcher() throws IOException {
		selector = Selector.open();
	}
	
	public SelectionKey registerChannel(SelectableChannel channel, ChannelProcessor proc, int interestOps) throws Exception {
		channel.configureBlocking(false);
		try {
			SelectionKey key = channel.register(selector, interestOps, proc);
			return key;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void run() throws IOException {
		running = true;
		while (running) {
			int num = selector.select();
			if (num == 0) continue;
			
			// We have got some data. Time for work
			Set<SelectionKey> keys = selector.selectedKeys();
			Iterator<SelectionKey> it = keys.iterator();
			while (it.hasNext()) {
				SelectionKey key = (SelectionKey)it.next();
				SelectableChannel channel = key.channel();
				if (key.attachment() instanceof ChannelProcessor) {
					ChannelProcessor proc = (ChannelProcessor) key.attachment();
					proc.process(channel, key);
				} else {
					throw new UnsupportedClassVersionError("The key attachment must have channel processor.");
				}
			}
			keys.clear();
		}
	}
	
	public void stop() {
		running = false;
	}
}
