package org.paititi.utils;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.DatagramChannel;
import java.nio.channels.MembershipKey;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;

import org.paititi.utils.MulticastDataLogger.FileNamerUtil.FileNamerMode;

public class MulticastDataLogger {
	public static class FileNamerUtil {
		public enum FileNamerMode {
			PORT,
			IP_PORT,
		}
		
		public static String getFileName(IPv4Address addr, String outFilePrefix, FileNamerMode mode) {
			String file = outFilePrefix;
			switch (mode) {
			case PORT:
				file += "." + addr.port() + ".dat";
				break;
			case IP_PORT:
				file += "." + addr.address().replaceAll(".", "_") + "." + addr.port() + ".dat";
				break;
			default:
				return "/dev/null";
			}
			return file;
		}
	}
	
    private ArrayList<DatagramChannel> multicastChannels;
    SelectDispatcher                   selectDispatcher;
    String                             outFilePrefix;
	FileNamerMode                      namer_mode;
    
    class DatagramChannelProcessor implements SelectDispatcher.ChannelProcessor {
    	private final String outputFile;
    	private final DataOutputStream outStream;
    	private ByteBuffer buf;
    	
    	public DatagramChannelProcessor(IPv4Address address, String outFilePrefix, FileNamerMode mode) throws FileNotFoundException {
    		outputFile = FileNamerUtil.getFileName(address, outFilePrefix, mode);
    		outStream = new DataOutputStream(new FileOutputStream(outputFile));
    		//TODO: Use something else in place of 16000
    		buf = ByteBuffer.allocate(16000);
    	}
    	
    	public String getOutPutFile() {
    		return outputFile;
    	}
    	
    	public void process(Channel channel, SelectionKey key) {
    		if (key.isReadable()) {
    			buf.clear();
    			try {
					((DatagramChannel)channel).receive(buf);
					outStream.write(buf.array());
				} catch (IOException e) {
					//TODO: What to do here?
					e.printStackTrace();
				}
    		}
    	}
    }
    
    /*
     * Assumption:
     * Please note that the namer must be set such that all channel's log files have different names
     */
    public MulticastDataLogger(ArrayList<String> mcastChannels, String outFilePrefix, FileNamerMode mode) throws Exception {
    	selectDispatcher = new SelectDispatcher();
    	for (String addr : mcastChannels) {
    		IPv4Address address = new IPv4Address(addr);
    		registerChannelForLogging(address);
    	}
    	this.outFilePrefix = outFilePrefix;
    	this.namer_mode = mode;
    }
    
    public void registerChannelForLogging(IPv4Address address) throws Exception {
    	
    	// Middle class people have only one interface. Will change when we become rich.
        NetworkInterface ni = NetworkInterface.getNetworkInterfaces().nextElement();

        DatagramChannel dc = DatagramChannel.open(StandardProtocolFamily.INET)
            .setOption(StandardSocketOptions.SO_REUSEADDR, true)
            .bind(new InetSocketAddress(address.port()))
            .setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
        InetAddress group = InetAddress.getByName(address.address());
        @SuppressWarnings("unused")
		MembershipKey key = dc.join(group, ni);
        
        DatagramChannelProcessor proc = new DatagramChannelProcessor(address, outFilePrefix, namer_mode);
        
        multicastChannels.add(dc);
        selectDispatcher.registerChannel(dc, proc, SelectionKey.OP_READ);
    }
	
    public void start() throws IOException {
    	// In place of a posix SIGINT handler, we try to add a shutdown hook
    	Runtime.getRuntime().addShutdownHook( new Thread(){
    		@Override
    		public void run() {
    			selectDispatcher.stop();
    	    }
    	} );
    	//Now only the dispatcher will run in a while loop
    	selectDispatcher.run();
    }
}
