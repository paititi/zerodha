package org.paititi.utils;

public class IPv4Address {
   private String address;
   private int port;
   
   public IPv4Address(String address, int port) {
	   this.address = address;
	   this.port = port;
   }
   
   public IPv4Address(String addr_port) throws InstantiationException {
	   String [] split = addr_port.split(":");
	   if (split.length == 2) {
		   this.address = split[0];
		   this.port = Integer.valueOf(split[1]);
	   } else {
		   throw new InstantiationException("Invalid IPv4 Address specified. Failed to parse.");
	   }
   }
   
   public int port() {
	   return port;
   }
   
   public String address() {
	   return address;
   }
   
   public String toString() {
	   return address + port;
   }
}
