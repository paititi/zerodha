package org.paititi.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.paititi.messages.*;
import org.paititi.utils.DataLogger.DataHeader;

import com.google.protobuf.GeneratedMessage.Builder;
import com.google.protobuf.Message;

public class DataReader {

	private final String inFile;
	private final DataInputStream inputStream;
	private DataHeader previousDataHeader;
	private final Builder<?> builder;

	public DataReader(String inFile, Builder<?> builder)
			throws FileNotFoundException {
		this.inFile = inFile;
		this.inputStream = new DataInputStream(new BufferedInputStream(
				new FileInputStream(inFile), 1024 * 1024));
		this.builder = builder;
	}

	public boolean hasNext() {
		try {
			DataHeader dataHeader = new DataHeader();
			dataHeader.readFields(inputStream);
			previousDataHeader = dataHeader;
		} catch (EOFException e) {
			return false;
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		return true;
	}

	public Message getNextMessage() throws IOException {
		if (previousDataHeader != null) {
			byte[] data = new byte[previousDataHeader.getNumBytes()];
			inputStream.read(data);
			builder.clear();
			return builder.mergeFrom(data).build();
		}
		return null;
	}

	public DataInputStream getInputStream() {
		return inputStream;
	}

	public String getInFile() {
		return inFile;
	}

	public static void main(String[] args) throws Exception {
		String dir = "/home/ankush/zerodha_data/2014-06-19/";
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		Map<String, String> tree = new TreeMap<String, String>();

		for (File file : new File(dir).listFiles()) {
			DataReader reader = new DataReader(file.getAbsolutePath()
					+ "/book.dat", MarketDataL2Message.book.newBuilder());
			Map<String, Double> map = new HashMap<String, Double>();
			while (reader.hasNext()) {
				MarketDataL2Message.book book;
				try {
					book = (MarketDataL2Message.book) reader.getNextMessage();
					calendar.setTimeInMillis(book.getTime() - 330 * 60 * 1000L);
					String key = simpleDateFormat.format(calendar.getTime());
					if (map.containsKey(key)) {
						map.put(key, Math.max(book.getPreviousClosePrice(), map.get(key)));
					} else {
						map.put(key, book.getPreviousClosePrice());
					}
				
				} catch (Exception e) {
				}
			}

			for (String key : map.keySet()) {
				if (tree.containsKey(key)) {
					tree.put(key, tree.get(key) + "," + map.get(key));
				} else {
					tree.put(key, map.get(key) + "");
				}
			}
		}

		PrintWriter printWriter = new PrintWriter("/tmp/data1.csv");
		for (String key : tree.keySet()) {
			printWriter.println(key + "," + tree.get(key));
		}
		printWriter.close();

		// double[] diff = new double[list.size()];
		// int i = 0;
		// for(Double d : list) {
		// diff[i++] = d;
		// }
		//
		// NumberFormat nf = NumberFormat.getInstance();
		// nf.setMaximumFractionDigits(2);
		//
		// System.out.println("50th " + nf.format(new
		// Percentile().evaluate(diff, 50)));
		// System.out.println("75th " + nf.format(new
		// Percentile().evaluate(diff, 75)));
		// System.out.println("95th " + nf.format(new
		// Percentile().evaluate(diff, 95)));
		// System.out.println("99th " + nf.format(new
		// Percentile().evaluate(diff, 99)));

	}
}
