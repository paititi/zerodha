package org.paititi.trade;

import java.util.ArrayList;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CollectingAlertHandler;
import com.gargoylesoftware.htmlunit.ConfirmHandler;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.PromptHandler;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class ZerodhaOrderRouter {

	public static void main(String[] args) throws Exception {
		String url = "https://trade.zerodha.com//index.html";
		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		webClient.getOptions().setAppletEnabled(true);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setRedirectEnabled(true);
		webClient.getOptions().setPopupBlockerEnabled(false);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		ArrayList<String> alerts = new ArrayList<String>();
		webClient.setAlertHandler(new CollectingAlertHandler(alerts));
		webClient.setPromptHandler(new PromptHandler() {

			public String handlePrompt(Page page, String message) {
				System.out.println("Prompt");
				return "OK";
			}
		});
		webClient.setConfirmHandler(new ConfirmHandler() {

			public boolean handleConfirm(Page page, String message) {
				System.out.println("Confirm");
				return true;
			}
		});
		HtmlPage loginPage = (HtmlPage) webClient.getPage(url);
		HtmlTextInput loginInput = (HtmlTextInput) loginPage.getElementsByName("requiredLogin").get(1);
		loginInput.setValueAttribute("<Set your Id here>");
		HtmlButtonInput loginButton = (HtmlButtonInput) loginPage.getElementByName("Sign In");
		loginPage = (HtmlPage) loginButton.click();
		HtmlCheckBoxInput checkBoxInput = (HtmlCheckBoxInput) loginPage.getElementByName("CorrectImage");
		loginPage = (HtmlPage) checkBoxInput.setChecked(true);
		HtmlPasswordInput passwordInput = (HtmlPasswordInput) loginPage.getElementByName("psswrd");
		passwordInput.setValueAttribute("<Set your password here>");
		loginButton = (HtmlButtonInput) loginPage.getByXPath("/html/body/center[2]/input[1]").get(0);
		ScriptResult result = loginPage.executeJavaScript("JavaScript:fun_auth_pwdLogin();");
		webClient.waitForBackgroundJavaScript(1000);
		loginPage = (HtmlPage) result.getNewPage();
		loginPage = (HtmlPage) loginButton.dblClick();
		HtmlPasswordInput answer1 = (HtmlPasswordInput) loginPage.getElementById("Answer1");
		answer1.setValueAttribute("<Set your answer here>");
		HtmlPasswordInput answer2 = (HtmlPasswordInput) loginPage.getElementById("Answer2");
		answer2.setValueAttribute("<Set your answer here>");
		loginButton = (HtmlButtonInput) loginPage.getElementByName("Continue");
		result = loginPage.executeJavaScript("javascript:func_auth_answers();");
		webClient.waitForBackgroundJavaScript(1000);
		loginPage = (HtmlPage) result.getNewPage();
		loginPage = (HtmlPage) loginButton.dblClick();
		loginButton = (HtmlButtonInput) loginPage.getElementByName("button_ok");
		HtmlPage welcomePage = (HtmlPage) loginButton.dblClick();
		HtmlPage buyPage = (HtmlPage) webClient
				.getPage("https://trade.zerodha.com/Zerodha/Trade/PlaceOrderSearch.jsp?Exchange=nse_cm&group=EQ&searchfor=PNB-EQ&sType=BUY");
		HtmlTextInput quantityInput = (HtmlTextInput) buyPage.getElementByName("Quantity");
		quantityInput.setValueAttribute("100");
		HtmlTextInput priceInput = (HtmlTextInput) buyPage.getElementByName("Price");
		priceInput.setValueAttribute("900");
		HtmlButtonInput submitInput = (HtmlButtonInput) buyPage.getElementByName("Submit322");
		HtmlPage confirmPage = (HtmlPage) submitInput.dblClick();
		System.out.println(confirmPage.getUrl());
		// passwordInput = (HtmlPasswordInput) confirmPage.getFocusedElement();
		// HtmlPage orderPage = (HtmlPage) webClient.getPage(confirmPage.getUrl());
		// System.out.println(orderPage.asXml());
	}

}
