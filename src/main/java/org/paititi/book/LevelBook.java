package org.paititi.book;

import java.util.ArrayList;

public class LevelBook {
	public class BookHalfLevel {
		private int      numOrders;
		private long     qty;
		private double   px;
	}
	
	public class LevelBookHalf {
		private ArrayList<BookHalfLevel> levelHalf;
	}
    
	int security_id;
	LevelBookHalf bid_levels;
	LevelBookHalf ask_levels;
	
	long    total_bid_qty;
	long    total_ask_qty;
	long    last_l1_update_time;
	long    last_l2_update_time; 
}
