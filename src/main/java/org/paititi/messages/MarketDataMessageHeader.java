package org.paititi.messages;

public class MarketDataMessageHeader {
	public enum MessageType {
		ALERT,
		STATUS_UPDATE,
		L1_SNAPSHOT,
		L2_SNAPSHOT
	}

	public MessageType   type;
	public int           len;
}
